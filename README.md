# Gameofthree Server
A Spring Boot WebSocket app for the main server.

# Start
There is a `Dockerfile` which packages the app, runs the tests and then starts it. It can be run via the `run.sh` file.
