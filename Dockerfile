FROM openjdk:14

COPY . .
RUN ["./mvnw", "package"]

ENTRYPOINT ["java", "-jar", "./target/gameofthree-0.0.1-SNAPSHOT.jar"]
