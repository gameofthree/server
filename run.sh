#!/bin/sh

PORT="80"
if [ -n "$1" ]; then
  PORT="$1"
fi

IMAGE_NAME="gameofthree-server"
if [ -n "$2" ]; then
  IMAGE_NAME="$2"
fi

CONTAINER_NAME="$IMAGE_NAME"

SCHEDULING_POOL_SIZE="8"
if [ -n "$3" ]; then
  SCHEDULING_POOL_SIZE="$3"
fi

docker container rm -f "$CONTAINER_NAME"
docker image rm -f "$IMAGE_NAME"

docker network create --driver bridge reverse_proxy_network || true
docker build -t "$IMAGE_NAME" .
docker run --network reverse_proxy_network --expose "$PORT" -e PORT="$PORT" -e SCHEDULING_POOL_SIZE="$SCHEDULING_POOL_SIZE" --name "$CONTAINER_NAME" "$IMAGE_NAME"
