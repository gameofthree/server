package bg.codexio.gameofthree.core.domain;

import bg.codexio.gameofthree.core.domain.exception.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyByte;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class RoomTests {

    @Test
    public void testInit_firstPlayer_expectedCurrentSide() {
        var player = new Player("1");
        var room = new Room("1", player);
        Assertions.assertEquals(player, room.getCurrentSide());
    }

    @Test
    public void testInit_firstPlayer_expectedNoOpponent() {
        var player = new Player("1");
        var room = new Room("1", player);
        Assertions.assertNull(room.getOpponent());
    }

    @Test
    public void testJoin_secondPlayer_expectedOpponent() throws FullRoomException {
        var playerOne = new Player("1");
        var playerTwo = new Player("2");
        var room = new Room("1", playerOne);

        room.join(playerTwo);

        Assertions.assertEquals(playerTwo, room.getOpponent());
    }

    @Test
    public void testJoin_thirdPlayer_expectedFullRoomException() throws FullRoomException {
        var playerOne = new Player("1");
        var playerTwo = new Player("2");
        var playerThree = new Player("3");
        var room = new Room("1", playerOne);
        room.join(playerTwo);

        Assertions.assertThrows(FullRoomException.class, () -> room.join(playerThree));
    }

    @Test
    public void testMove_validData_expectedMove() throws PlayerNotOnTurnException,
            InvalidMoveException,
            RoomAlreadyFinishedException,
            NoOpponentException,
            FullRoomException {
        var playerOne = new Player("1");
        var playerTwo = new Player("2");
        var room = new Room("1", playerOne);

        room.join(playerTwo);

        var move = room.initMove(playerOne.getId(), (byte) 1);

        Assertions.assertNotNull(move);
    }

    @Test
    public void testMove_validData_expectedSwitchedSide() throws PlayerNotOnTurnException,
            InvalidMoveException,
            RoomAlreadyFinishedException,
            NoOpponentException,
            FullRoomException {
        var playerOne = new Player("1");
        var playerTwo = new Player("2");
        var room = new Room("1", playerOne);

        room.join(playerTwo);

        room.initMove(playerOne.getId(), (byte) 1);

        Assertions.assertEquals(playerTwo, room.getCurrentSide());
    }

    @Test
    public void testMove_notOnTurn_expectedNotOnTurnException() throws FullRoomException {
        var playerOne = new Player("1");
        var playerTwo = new Player("2");
        var room = new Room("1", playerOne);

        room.join(playerTwo);

        Assertions.assertThrows(PlayerNotOnTurnException.class, () -> room.initMove(playerTwo.getId(), (byte) 1));
    }

    @Test
    public void testMove_noOpponent_expectedNoOpponentException() {
        var playerOne = new Player("1");
        var room = new Room("1", playerOne);

        Assertions.assertThrows(NoOpponentException.class, () -> room.initMove(playerOne.getId(), (byte) 1));
    }

    @Test
    public void testMove_alreadyWon_expectedAlreadyFinishedException() throws FullRoomException {
        var playerOne = new Player("1");
        var playerTwo = new Player("2");
        var room = new Room("1", playerOne);

        room.join(playerTwo);
        room.setWinner(playerOne);

        Assertions.assertThrows(RoomAlreadyFinishedException.class, () -> room.initMove(playerOne.getId(), (byte) 1));
    }

    @Test
    public void testMove_winningMove_expectedWinner()
            throws FullRoomException,
            InvalidMoveException,
            NoOpponentException,
            PlayerNotOnTurnException,
            RoomAlreadyFinishedException {
        var playerOne = Mockito.mock(Player.class);
        lenient().when(playerOne.getId()).thenReturn("1");
        lenient().when(playerOne.move(any(), anyByte())).thenReturn(new Move("1", playerOne, 4, (byte) -1));
        var playerTwo = new Player("2");
        var room = new Room("1", playerOne);

        room.join(playerTwo);

        room.initMove(playerOne.getId(), (byte) 1);

        Assertions.assertEquals(playerOne, room.getWinner());
    }
}
