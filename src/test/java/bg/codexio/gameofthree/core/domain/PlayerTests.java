package bg.codexio.gameofthree.core.domain;

import bg.codexio.gameofthree.core.domain.exception.InvalidMoveException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PlayerTests {

    @Test
    void testMove_noPreviousMoves_expectedNewMove() throws InvalidMoveException {
        var player = new Player("1");
        var move = player.move(null, (byte) 0);
        Assertions.assertNotNull(move);
    }

    @Test
    void testMove_withPreviousMove_expectedCalculatedNumber() throws InvalidMoveException {
        var player = new Player("1");
        var previous = new Move("1", player, 8, (byte) 1);
        var move = player.move(previous, (byte) 0);
        try {
            move.calculateNewNumber();
        } catch (InvalidMoveException ex) {
            Assertions.fail(ex.getClass().getSimpleName());
        }
    }

    @Test
    void testMove_withInvalidModifierForCalculation_expectedInvalidMoveException() throws InvalidMoveException {
        var player = new Player("1");
        var previous = new Move("1", player, 8, (byte) 1);
        var move = player.move(previous, (byte) 1);
        Assertions.assertThrows(InvalidMoveException.class, move::calculateNewNumber);
    }

    @Test
    void testMove_withInvalidModifierForRange_expectedInvalidMoveException() {
        var player = new Player("1");
        var previous = new Move("1", player, 8, (byte) 1);
        Assertions.assertThrows(InvalidMoveException.class, () -> player.move(previous, (byte) 8));
    }
}
