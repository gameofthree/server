package bg.codexio.gameofthree.communication.config;

import bg.codexio.gameofthree.communication.annotation.WebSocketListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    private final ApplicationContext context;

    public WebSocketConfig(ApplicationContext context) {
        this.context = context;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
        this.context.getBeansWithAnnotation(WebSocketListener.class).forEach((ignored, beanInstance) ->
                webSocketHandlerRegistry.addHandler(
                        (WebSocketHandler) beanInstance,
                        beanInstance.getClass().getAnnotation(WebSocketListener.class).value()
                )
        );
    }

}
