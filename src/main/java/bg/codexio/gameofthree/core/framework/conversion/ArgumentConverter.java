package bg.codexio.gameofthree.core.framework.conversion;

import bg.codexio.gameofthree.core.framework.exception.ArgumentConversionException;

import java.util.function.Function;

public interface ArgumentConverter {

    Object convert(String source, Class<?> target) throws ArgumentConversionException;

    <T> void addConversion(Class<T> source, Function<String, T> converter);

}
