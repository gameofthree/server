package bg.codexio.gameofthree.core.framework.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Component
@Order(Integer.MIN_VALUE)
public class RespondToAwareScheduledAnnotationPostProcessor extends ScheduledAnnotationBeanPostProcessor {

    private ApplicationEventPublisher publisher;

    public RespondToAwareScheduledAnnotationPostProcessor() {
    }

    public RespondToAwareScheduledAnnotationPostProcessor(ScheduledTaskRegistrar registrar) {
        super(registrar);
    }

    @Autowired
    public void setPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    @Override
    protected Runnable createRunnable(Object target, Method method) {
        return new RespondToAwareScheduledMethodRunnable(target, method, this.publisher);
    }

}
