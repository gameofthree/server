package bg.codexio.gameofthree.core.framework.config;

import bg.codexio.gameofthree.core.framework.schedule.RespondToAwareScheduledAnnotationPostProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.ScheduledAnnotationBeanPostProcessor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.config.ScheduledTaskHolder;

import java.util.Arrays;

@Configuration
@EnableScheduling
public class SchedulingConfig {

    private final int poolSize;

    public SchedulingConfig(@Value("${spring.task.scheduling.pool.size}") int poolSize, ApplicationContext context) {
        this.poolSize = poolSize;
        Arrays.stream(context.getBeanNamesForType(ScheduledTaskHolder.class)).forEach(b -> {
            ((BeanDefinitionRegistry) context.getAutowireCapableBeanFactory()).removeBeanDefinition(b);
        });
    }

    @Bean(name = "org.springframework.context.annotation.internalScheduledAnnotationProcessor")
    public ScheduledAnnotationBeanPostProcessor scheduledAnnotationBeanPostProcessor() {
        return new RespondToAwareScheduledAnnotationPostProcessor();
    }

    @Bean
    public TaskScheduler taskScheduler() {
        var scheduler = new ThreadPoolTaskScheduler();

        scheduler.setPoolSize(this.poolSize);
        scheduler.setThreadNamePrefix("scheduled-task-");
        scheduler.setDaemon(true);

        return scheduler;
    }

}
