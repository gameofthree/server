package bg.codexio.gameofthree.core.framework.conversion;

import bg.codexio.gameofthree.core.framework.exception.ArgumentConversionException;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class DefaultArgumentConverter implements ArgumentConverter {

    private static final Map<Class<?>, Function<String, ?>> INITIAL_CONVERSIONS = Map.of(
            String.class, s -> s,
            Date.class, s -> {
                try {
                    return new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(s);
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            },
            Long.class, Long::parseLong,
            long.class, Long::parseLong,
            Integer.class, Integer::parseInt,
            int.class, Integer::parseInt
    );

    private final Map<Class<?>, Function<String, ?>> conversion;

    public DefaultArgumentConverter() {
        this.conversion = new HashMap<>();
        INITIAL_CONVERSIONS.forEach(this.conversion::put);
    }

    @Override
    public Object convert(String source, Class<?> target) throws ArgumentConversionException {
        if (!this.conversion.containsKey(target)) {
            throw new ArgumentConversionException();
        }

        return this.conversion.get(target).apply(source);
    }

    @Override
    public <T> void addConversion(Class<T> source, Function<String, T> converter) {
        this.conversion.put(source, converter);
    }

}
