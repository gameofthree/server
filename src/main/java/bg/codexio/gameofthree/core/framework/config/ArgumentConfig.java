package bg.codexio.gameofthree.core.framework.config;

import bg.codexio.gameofthree.core.framework.conversion.ArgumentConverter;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ArgumentConfig {

    public ArgumentConfig(ArgumentConverter argumentConverter) {
        argumentConverter.addConversion(Byte.class, Byte::parseByte);
        argumentConverter.addConversion(byte.class, Byte::parseByte);
    }

}
