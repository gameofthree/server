package bg.codexio.gameofthree.core.framework.conversion;

import bg.codexio.gameofthree.core.framework.exception.MalformedMessageException;
import bg.codexio.gameofthree.core.framework.exception.MessageConversionException;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;

@Component
public class KeyValueDispatchingConversionStrategy
        implements MessageConversionStrategy<KeyValueDispatchingConversionStrategy.KeyValuePair, String, TextMessage> {


    @Override
    public KeyValuePair execute(TextMessage message) throws MessageConversionException {
        var pair = message.getPayload().split(":");
        if (pair.length != 2) {
            throw new MalformedMessageException();
        }

        return new KeyValuePair(pair[0], pair[1]);
    }

    public static class KeyValuePair {
        private final String key;

        private final String value;

        public KeyValuePair(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }

}
