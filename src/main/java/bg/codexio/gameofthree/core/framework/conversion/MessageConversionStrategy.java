package bg.codexio.gameofthree.core.framework.conversion;

import bg.codexio.gameofthree.core.framework.exception.MessageConversionException;
import org.springframework.web.socket.WebSocketMessage;

public interface MessageConversionStrategy<T, U, V extends WebSocketMessage<U>> {

    T execute(V message) throws MessageConversionException;

}
