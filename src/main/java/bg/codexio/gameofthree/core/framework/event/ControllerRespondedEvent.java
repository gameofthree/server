package bg.codexio.gameofthree.core.framework.event;

import bg.codexio.gameofthree.core.framework.data.MessageEntity;

public class ControllerRespondedEvent {

    private final String channel;

    private final MessageEntity<?> messageEntity;

    public ControllerRespondedEvent(String channel, MessageEntity<?> messageEntity) {
        this.channel = channel;
        this.messageEntity = messageEntity;
    }

    public String getChannel() {
        return channel;
    }

    public MessageEntity<?> getMessageEntity() {
        return messageEntity;
    }

}
