package bg.codexio.gameofthree.core.framework.data;

public class MessageEntity<T> {

    private final String[] receivingSides;

    private final T payload;

    MessageEntity(T payload, String... receivingSides) {
        this.receivingSides = receivingSides;
        this.payload = payload;
    }

    public static MessageEntityBuilder of(String... receivingSides) {
        return new MessageEntityBuilder(receivingSides);
    }

    public String[] getReceivingSides() {
        return receivingSides;
    }

    public T getPayload() {
        return payload;
    }

}
