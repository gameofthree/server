package bg.codexio.gameofthree.core.framework.data;

public class MessageEntityBuilder {

    private final String[] receivingSides;

    MessageEntityBuilder(String... receivingSides) {
        this.receivingSides = receivingSides;
    }

    public <T> MessageEntity<T> withPayload(T payload) {
        return new MessageEntity<>(payload, this.receivingSides);
    }

}
