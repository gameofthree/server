package bg.codexio.gameofthree.core.framework.data;

public class EventMessage {

    private final String channel;

    private final Object payload;

    public EventMessage(String channel, Object payload) {
        this.channel = channel;
        this.payload = payload;
    }

    public String getChannel() {
        return channel;
    }

    public Object getPayload() {
        return payload;
    }

}
