package bg.codexio.gameofthree.core.framework.listener;

import bg.codexio.gameofthree.communication.annotation.WebSocketListener;
import bg.codexio.gameofthree.core.framework.annotation.*;
import bg.codexio.gameofthree.core.framework.conversion.ArgumentConverter;
import bg.codexio.gameofthree.core.framework.conversion.KeyValueDispatchingConversionStrategy;
import bg.codexio.gameofthree.core.framework.conversion.MessageConversionStrategy;
import bg.codexio.gameofthree.core.framework.data.EventMessage;
import bg.codexio.gameofthree.core.framework.data.MessageEntity;
import bg.codexio.gameofthree.core.framework.event.ControllerRespondedEvent;
import bg.codexio.gameofthree.core.framework.exception.ArgumentConversionException;
import bg.codexio.gameofthree.core.framework.exception.MessageConversionException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@WebSocketListener("/**")
@Component
public class DispatcherListener extends TextWebSocketHandler {

    private final MessageConversionStrategy<KeyValueDispatchingConversionStrategy.KeyValuePair, String, TextMessage> strategy;

    private final Map<String, WebSocketSession> sessionsById;

    private final Map<String, ControllerActionPair> cachedControllers;

    private final ApplicationContext context;

    private final ArgumentConverter argumentConverter;

    private final ObjectMapper objectMapper;

    public DispatcherListener(
            MessageConversionStrategy<KeyValueDispatchingConversionStrategy.KeyValuePair, String, TextMessage> strategy,
            ApplicationContext context,
            ArgumentConverter argumentConverter,
            ObjectMapper objectMapper
    ) {
        this.strategy = strategy;
        this.context = context;
        this.argumentConverter = argumentConverter;
        this.objectMapper = objectMapper;
        this.cachedControllers = new ConcurrentHashMap<>();
        this.sessionsById = new ConcurrentHashMap<>();
    }

    public WebSocketSession getSessionById(String id) {
        return this.sessionsById.get(id);
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws IllegalAccessException, IOException {
        this.sessionsById.putIfAbsent(session.getId(), session);

        try {
            var messagePair = this.strategy.execute(message);

            var ctrlActionPair = this.getControllerActionPair(messagePair);

            if (ctrlActionPair == null || messagePair.getKey() == null) {
                return;
            }

            this.cachedControllers.putIfAbsent(messagePair.getKey(), ctrlActionPair);

            var ctrl = ctrlActionPair.getController();
            var action = ctrlActionPair.getAction();

            var params = this.determineParams(session, messagePair, action);

            var res = action.invoke(ctrl, params);

            this.sendToResponseChannel(action, res);
        } catch (MessageConversionException | ArgumentConversionException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            this.send(session.getId(), "BAD_REQUEST", e.getCause().getClass().getSimpleName());
        }
    }

    @EventListener(ControllerRespondedEvent.class)
    public void bulkSend(ControllerRespondedEvent response) {
        Arrays.stream(response.getMessageEntity().getReceivingSides())
                .forEach(receiver -> this.send(
                        receiver,
                        response.getChannel(),
                        response.getMessageEntity().getPayload()
                ));
    }


    private void sendToResponseChannel(Method action, Object res) {
        if (action.isAnnotationPresent(RespondTo.class) && res instanceof MessageEntity) {
            var channel = action.getAnnotation(RespondTo.class).value();
            this.bulkSend(new ControllerRespondedEvent(channel, (MessageEntity<?>) res));
        }
    }

    private void send(String receiver, String channel, Object message) {
        try {
            var session = this.sessionsById.get(receiver);
            if (session == null || !session.isOpen()) {
                this.sessionsById.remove(receiver);
                return;
            }

            this.sessionsById.get(receiver).sendMessage(
                    new TextMessage(
                            this.objectMapper.writeValueAsString(
                                    new EventMessage(channel, message)
                            )
                    )
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ControllerActionPair getControllerActionPair(KeyValueDispatchingConversionStrategy.KeyValuePair messagePair) {
        return this.cachedControllers.getOrDefault(
                messagePair.getKey(),
                this.context.getBeansWithAnnotation(GameController.class)
                        .values()
                        .stream()
                        .map(ctrl -> new ControllerActionPair(
                                        ctrl,
                                        Arrays.stream(ctrl.getClass().getDeclaredMethods())
                                                .filter(m -> m.isAnnotationPresent(GameOperation.class))
                                                .filter(m -> m.getAnnotation(GameOperation.class).value().equals(messagePair.getKey()))
                                                .findFirst()
                                                .orElse(null)
                                )
                        )
                        .filter(ctrl -> ctrl.getAction() != null)
                        .findFirst()
                        .orElse(null)
        );
    }

    private Object[] determineParams(WebSocketSession session, KeyValueDispatchingConversionStrategy.KeyValuePair messagePair, Method action) throws ArgumentConversionException {
        var params = new Object[action.getParameterCount()];
        for (var i = 0; i < params.length; i++) {
            var paramInfo = action.getParameters()[i];
            if (paramInfo.isAnnotationPresent(UserId.class)) {
                params[i] = session.getId();
            } else if (paramInfo.isAnnotationPresent(MessageValue.class)) {
                params[i] = this.argumentConverter.convert(messagePair.getValue(), paramInfo.getType());
            }
        }
        return params;
    }

    static class ControllerActionPair {
        private final Object controller;

        private final Method action;

        public ControllerActionPair(Object controller, Method action) {
            this.controller = controller;
            this.action = action;
        }

        public Object getController() {
            return controller;
        }

        public Method getAction() {
            return action;
        }
    }

}
