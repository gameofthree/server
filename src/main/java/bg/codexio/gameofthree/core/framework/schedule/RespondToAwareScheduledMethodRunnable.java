package bg.codexio.gameofthree.core.framework.schedule;

import bg.codexio.gameofthree.core.framework.annotation.RespondTo;
import bg.codexio.gameofthree.core.framework.data.MessageEntity;
import bg.codexio.gameofthree.core.framework.event.ControllerRespondedEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.support.ScheduledMethodRunnable;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;

public class RespondToAwareScheduledMethodRunnable extends ScheduledMethodRunnable {

    private ApplicationEventPublisher publisher;

    public RespondToAwareScheduledMethodRunnable(Object target, Method method, ApplicationEventPublisher publisher) {
        super(target, method);
        this.publisher = publisher;
    }

    public RespondToAwareScheduledMethodRunnable(Object target, String methodName) throws NoSuchMethodException {
        super(target, methodName);
    }

    @Override
    public void run() {
        try {
            ReflectionUtils.makeAccessible(this.getMethod());
            var result = this.getMethod().invoke(this.getTarget());
            if (!this.getMethod().isAnnotationPresent(RespondTo.class)) {
                return;
            }
            var channel = this.getMethod().getAnnotation(RespondTo.class).value();
            if (result instanceof MessageEntity) {
                this.publishRespondToEvent((MessageEntity<?>) result, channel);
            }

            if (result instanceof Iterable) {
                ((Iterable<?>) result).forEach(item -> {
                    if (item instanceof MessageEntity) {
                        this.publishRespondToEvent((MessageEntity<?>) item, channel);
                    }
                });
            }
        } catch (InvocationTargetException ex) {
            ReflectionUtils.rethrowRuntimeException(ex.getTargetException());
        } catch (IllegalAccessException ex) {
            throw new UndeclaredThrowableException(ex);
        }
    }

    private void publishRespondToEvent(MessageEntity<?> result, String channel) {
        this.publisher.publishEvent(
                new ControllerRespondedEvent(
                        channel,
                        result
                )
        );
    }

}
