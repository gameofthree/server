package bg.codexio.gameofthree.core.domain;

import bg.codexio.gameofthree.core.domain.exception.*;

import java.util.Date;
import java.util.LinkedList;

public class Room {

    public static final int IDLE_GAME_INTERVAL = 60 * 1000;

    private final String id;

    private final Player leftSide;

    private Player rightSide;

    private Player currentSide;

    private Player winner;

    private final LinkedList<Move> history;

    public Room(String id, Player leftSide) {
        this.id = id;
        this.leftSide = leftSide;
        this.currentSide = leftSide;
        this.history = new LinkedList<>();
    }

    public String getId() {
        return id;
    }

    public void join(Player player) throws FullRoomException {
        this.throwIfFullRoom();

        this.rightSide = player;
    }

    public Iterable<Move> getHistory() {
        return history;
    }

    public Move getLastMove() {
        return this.history.isEmpty() ? null : this.history.getLast();
    }

    public Player getCurrentSide() {
        return currentSide;
    }

    public Player getOpponent() {
        return this.getCurrentSide() == this.leftSide ? this.rightSide : this.leftSide;
    }

    public boolean isOnTurn(String id) {
        return this.getCurrentSide() != null && this.getCurrentSide().getId().equals(id);
    }

    public boolean isIdle() {
        if (this.history.isEmpty()) {
            return false;
        }

        var diff = new Date().getTime() - this.getLastMove().getCreatedOn().getTime();

        return diff > IDLE_GAME_INTERVAL;
    }

    public boolean hasWinner() {
        return this.winner != null;
    }

    public Player getWinner() {
        return winner;
    }

    public Move initMove(String id, byte modifier)
            throws PlayerNotOnTurnException,
            InvalidMoveException,
            RoomAlreadyFinishedException,
            NoOpponentException {
        this.throwIfNotOnTurn(id);
        this.throwIfNoOpponent();
        this.throwIfAlreadyWon();

        var move = this.currentSide.move(
                this.history.isEmpty() ? null : this.history.getLast(),
                modifier
        );

        if (this.isWinner(move)) {
            this.setWinningData();
        } else {
            this.setCurrentSide(this.getOpponent());
        }

        this.history.add(move);

        return move;
    }

    public boolean isFull() {
        return this.rightSide != null;
    }

    void setWinner(Player winner) {
        this.winner = winner;
    }

    private void setCurrentSide(Player currentSide) {
        this.currentSide = currentSide;
    }

    private void setWinningData() {
        this.setWinner(this.getCurrentSide());
    }

    private void throwIfNotOnTurn(String playerId) throws PlayerNotOnTurnException {
        if (!this.isOnTurn(playerId)) {
            throw new PlayerNotOnTurnException();
        }
    }

    private void throwIfAlreadyWon() throws RoomAlreadyFinishedException {
        if (this.hasWinner()) {
            throw new RoomAlreadyFinishedException();
        }
    }

    private void throwIfFullRoom() throws FullRoomException {
        if (this.isFull()) {
            throw new FullRoomException();
        }
    }

    private void throwIfNoOpponent() throws NoOpponentException {
        if (!this.isFull()) {
            throw new NoOpponentException();
        }
    }

    private boolean isWinner(Move move) throws InvalidMoveException {
        return move.calculateNewNumber() == 1;
    }

    public void tryWalkover() {
        if (this.isIdle()) {
            this.setWinner(this.getOpponent());
        }
    }

}
