package bg.codexio.gameofthree.core.domain;

import bg.codexio.gameofthree.core.domain.exception.InvalidMoveException;
import bg.codexio.gameofthree.core.domain.validation.MoveValidator;

import java.util.UUID;

public class Player {

    private static final int MAX_INCEPTION_NUMBER = Short.MAX_VALUE;

    private final String id;

    public Player(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public Move move(Move lastMove, byte modifier) throws InvalidMoveException {
        if (modifier < -1 || modifier > 1) {
            throw new InvalidMoveException();
        }

        return new Move(
                UUID.randomUUID().toString(),
                this,
                lastMove != null ? lastMove.calculateNewNumber() : this.getRandomNumberForModifier(modifier),
                modifier
        );
    }

    private int getRandomNumberForModifier(byte modifier) {
        int number, result;
        do {
            number = (int) (Math.random() * MAX_INCEPTION_NUMBER);
            result = (number + modifier) / MoveValidator.DEFAULT_DIVISOR;
        } while ((result * MoveValidator.DEFAULT_DIVISOR) - modifier != number);

        return number;
    }

}
