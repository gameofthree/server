package bg.codexio.gameofthree.core.domain;

import bg.codexio.gameofthree.core.domain.exception.InvalidMoveException;
import bg.codexio.gameofthree.core.domain.validation.MoveValidator;
import bg.codexio.gameofthree.core.domain.validation.Validator;

import java.util.Date;

public class Move {

    private final String id;

    private final Player player;

    private final int lastNumber;

    private final short modifier;

    private final Date createdOn;

    private Validator<Move, InvalidMoveException> validator;

    public Move(String id, Player player, int lastNumber, byte modifier) {
        this(id, player, lastNumber, modifier, null);
        this.validator = new MoveValidator(this);
    }

    public Move(
            String id,
            Player player,
            int lastNumber,
            byte modifier,
            Validator<Move, InvalidMoveException> validator
    ) {
        this.id = id;
        this.player = player;
        this.lastNumber = lastNumber;
        this.modifier = modifier;
        this.validator = validator;
        this.createdOn = new Date();
    }

    public String getId() {
        return id;
    }

    public Player getPlayer() {
        return player;
    }

    public int getLastNumber() {
        return lastNumber;
    }

    public short getModifier() {
        return modifier;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public int calculateNewNumber() throws InvalidMoveException {
        this.validator.validate();

        return (this.getLastNumber() + this.getModifier()) / MoveValidator.DEFAULT_DIVISOR;
    }

}
