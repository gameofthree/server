package bg.codexio.gameofthree.core.domain.validation;

import bg.codexio.gameofthree.core.framework.exception.ValidationException;

public interface Validator<T, U extends ValidationException> {

    void validate() throws U;

}
