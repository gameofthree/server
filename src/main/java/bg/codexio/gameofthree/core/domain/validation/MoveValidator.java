package bg.codexio.gameofthree.core.domain.validation;

import bg.codexio.gameofthree.core.domain.Move;
import bg.codexio.gameofthree.core.domain.exception.InvalidMoveException;

public class MoveValidator implements Validator<Move, InvalidMoveException> {

    public static final int DEFAULT_DIVISOR = 3;

    private final Move move;

    public MoveValidator(Move move) {
        this.move = move;
    }

    @Override
    public void validate() throws InvalidMoveException {
        var sum = this.move.getLastNumber() + this.move.getModifier();
        var realDivision = sum / (double) DEFAULT_DIVISOR;
        var floorDivision = (double) (sum / DEFAULT_DIVISOR);

        if (floorDivision != realDivision) {
            throw new InvalidMoveException();
        }
    }

}
