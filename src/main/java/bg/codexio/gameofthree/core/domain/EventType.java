package bg.codexio.gameofthree.core.domain;

public class EventType {

    public static class Input {
        public static final String JOIN_ROOM = "JOIN_ROOM";
        public static final String MOVE = "MOVE";
        public static final String IDLE_GAME = "IDLE_GAME";
    }

    public static class Output {
        public static final String ROOM_JOINED = "ROOM_JOINED";
        public static final String MOVE_FINISHED = "MOVE_FINISHED";
    }

}
