package bg.codexio.gameofthree.core.domain.controller;

import bg.codexio.gameofthree.core.domain.EventType;
import bg.codexio.gameofthree.core.domain.Player;
import bg.codexio.gameofthree.core.domain.Room;
import bg.codexio.gameofthree.core.domain.exception.*;
import bg.codexio.gameofthree.core.framework.annotation.*;
import bg.codexio.gameofthree.core.framework.data.MessageEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@GameController
@Component
public class MainController {

    private static final byte[] POSSIBLE_MODIFIERS = {-1, 0, 1};

    private final Map<String, Player> playersBySessionId;

    private final Map<String, Room> roomsBySessionId;

    private volatile Room lastRoom;

    public MainController() {
        this.playersBySessionId = new ConcurrentHashMap<>();
        this.roomsBySessionId = new ConcurrentHashMap<>();
        this.lastRoom = null;
    }

    @GameOperation(EventType.Input.JOIN_ROOM)
    @RespondTo(EventType.Output.ROOM_JOINED)
    public MessageEntity<Room> joinRoom(@UserId String userId)
            throws FullRoomException,
            InvalidMoveException,
            NoOpponentException,
            RoomAlreadyFinishedException,
            PlayerNotOnTurnException {
        if (this.roomsBySessionId.containsKey(userId)) {
            return MessageEntity.of(this.getReceivingSide(userId)).withPayload(this.roomsBySessionId.get(userId));
        }

        this.playersBySessionId.putIfAbsent(userId, new Player(userId));

        if (this.lastRoom == null) {
            this.lastRoom = new Room(
                    UUID.randomUUID().toString(),
                    this.playersBySessionId.get(userId)
            );

            this.roomsBySessionId.put(userId, this.lastRoom);

            return MessageEntity.of(userId).withPayload(this.lastRoom);
        }

        this.roomsBySessionId.put(userId, this.lastRoom);
        this.lastRoom.join(this.playersBySessionId.get(userId));

        var opponentId = this.lastRoom.getCurrentSide().getId();

        this.lastRoom.initMove(
                opponentId,
                POSSIBLE_MODIFIERS[(int) (Math.random() * (POSSIBLE_MODIFIERS.length - 1))]
        );

        var room = this.lastRoom;
        this.lastRoom = null;

        return MessageEntity.of(opponentId, userId).withPayload(room);
    }

    @GameOperation(EventType.Input.MOVE)
    @RespondTo(EventType.Output.MOVE_FINISHED)
    public MessageEntity<Room> move(@UserId String userId, @MessageValue byte modifier)
            throws NotYourRoomException,
            RoomAlreadyFinishedException,
            PlayerNotOnTurnException,
            InvalidMoveException,
            NoOpponentException {
        if (!this.roomsBySessionId.containsKey(userId)) {
            throw new NotYourRoomException();
        }

        var room = this.roomsBySessionId.get(userId);

        room.initMove(userId, modifier);

        var responseBuilder = MessageEntity.of(this.getReceivingSide(userId), userId);

        if (room.hasWinner()) {
            this.cleanUpFinishedDuel(room);
        }

        return responseBuilder.withPayload(room);
    }

    @GameOperation(EventType.Input.IDLE_GAME)
    @RespondTo(EventType.Output.MOVE_FINISHED)
    public MessageEntity<Room> checkIdleGame(@UserId String userId) throws NotYourRoomException {
        if (!this.roomsBySessionId.containsKey(userId)) {
            throw new NotYourRoomException();
        }

        var room = this.roomsBySessionId.get(userId);
        var responseBuilder = MessageEntity.of(this.getReceivingSide(userId), userId);

        if (room.isIdle()) {
            room.tryWalkover();
            this.cleanUpFinishedDuel(room);
        }

        return responseBuilder.withPayload(room);
    }

    @Scheduled(fixedRate = Room.IDLE_GAME_INTERVAL / 2)
    @RespondTo(EventType.Output.MOVE_FINISHED)
    public List<MessageEntity<Room>> finishIdleGames() {
        var messages = new ArrayList<MessageEntity<Room>>();
        this.roomsBySessionId.forEach((sessionId, room) -> {
            if (room.isIdle()) {
                room.tryWalkover();
                messages.add(MessageEntity.of(room.getCurrentSide().getId(), room.getOpponent().getId()).withPayload(room));
                this.roomsBySessionId.remove(sessionId);
            }
        });

        return messages;
    }

    private String getReceivingSide(String userId) {
        var room = this.roomsBySessionId.get(userId);

        return room.getCurrentSide().getId().equals(userId)
                ? room.getOpponent().getId()
                : room.getCurrentSide().getId();
    }

    private void cleanUpFinishedDuel(Room room) {
        this.roomsBySessionId.remove(room.getOpponent().getId());
        this.roomsBySessionId.remove(room.getCurrentSide().getId());
    }

}
